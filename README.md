# PWA-Vue-X
## Setup
### Clone this repository
```
git clone https://gitlab.com/organizados/pwa-vue-x
```
### Install dependencies with [yarn](https://yarnpkg.com) package manager
```
yarn install
````
### Run development flow
Manually with vue-cli
```
vue-cli-service serve
```
Or selecting the project folder within VueUI
```
vue ui -H 0.0.0.0
```
## Idea
This repository is mean to be no more than a boilerplate, where:
 * Learn how to use npm scripts
 * Setup [CI/CD](https://docs.gitlab.com/ce/ci/)
 * Each stack implies a new branch
 * Implement different [MBaaS](https://en.wikipedia.org/wiki/Mobile_backend_as_a_service) providers

## Providers
 * [Auth0](https://auth0.com/)
 * [AWS-Cognito](https://aws.amazon.com/cognito/)
 * [Firebase](http://firebase.google.com/)
 * [Hasura](https://hasura.io/diy-graphql-baas)